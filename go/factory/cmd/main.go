package main

import (
	"factory/internal/factory"

	log "github.com/sirupsen/logrus"
)

func main() {
	car, err := factory.NewVehicule("car")
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("%s has been created", car.Name())

	moto, err := factory.NewVehicule("moto")
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("%s has been created", moto.Name())

	_, err = factory.NewVehicule("dummy")
	if err != nil {
		log.Error(err)
	}
}
