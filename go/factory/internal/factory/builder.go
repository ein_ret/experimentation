package factory

import (
	"factory/internal/entities"
	"fmt"

	log "github.com/sirupsen/logrus"
)

//////////////////////////////////////////////////////////////////////////////
// Variables and constants
//////////////////////////////////////////////////////////////////////////////

var (
	vehiculeFactories = make(map[string]vehiculeFactory)
)

//////////////////////////////////////////////////////////////////////////////
// Private structure declaration
//////////////////////////////////////////////////////////////////////////////

// vehiculeFactory represents the constructor, for the demo purpose, the constructor signature is trivial
type vehiculeFactory func() entities.Vehiculer

//////////////////////////////////////////////////////////////////////////////
// Private methods
//////////////////////////////////////////////////////////////////////////////

func register(name string, constructor vehiculeFactory) {
	if _, registered := vehiculeFactories[name]; registered {
		log.Errorf("%s already registered, ignored it", name)
	}
	vehiculeFactories[name] = constructor
}

//////////////////////////////////////////////////////////////////////////////
// Public methods
//////////////////////////////////////////////////////////////////////////////

// NewVehicule creates a vehicule based on the name parameter
func NewVehicule(name string) (entities.Vehiculer, error) {
	if constructor, registered := vehiculeFactories[name]; registered {
		return constructor(), nil
	}
	return nil, fmt.Errorf("%s is an invalid Vehicule", name)
}

// old version
// func NewVehicule(name string) entities.Vehiculer {
// 	switch name {
// 	case "car":
// 		return entities.NewCar()
// 	case "moto":
// 		return entities.NewMoto()
// 	default:
// 		fmt.Println("vehicule not supported")
// 		return nil
// 	}
// }
