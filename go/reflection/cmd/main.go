package main

import (
	"fmt"
	dynalib "junk/reflection/pkg"
	"reflect"

	log "github.com/sirupsen/logrus"
)

func reflecttry(namelibtoexecute string, args ...string) {
	val := reflect.ValueOf(&dynalib.Mylib{})         //It takes a pointer to the struct and return a Value of the receiver Type. (Here a struct Mylib)
	functocall := val.MethodByName(namelibtoexecute) //It returns a Value of the function which takes the same input / output that the real function

	if functocall.Kind() != reflect.Func { // Note it seems that function Lib1, Lib2 and Mylib struct have to be exported, otherwise it doesnot work with an error, does it means that we cannot reflect "private" function/struct ?
		log.Warnf("%v is not a function\n", namelibtoexecute)
		return
	}

	//Involve to have the same function type for the reflection function used
	functype := functocall.Interface().(func(strs []string)) //func (v Value) Interface() (i interface{})  returns v's current value as an interface{}. It is equivalent to: var i interface{} = (v's underlying value)
	functype(args)
}

func main() {

	fmt.Println("Begining...")
	reflecttry("Lib1", "a", "b")
	reflecttry("Lib3")
	reflecttry("Lib2", "c", "d", "e")

}
