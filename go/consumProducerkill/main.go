package main

import (
	"fmt"
	"strings"
	"sync"

	"context"

	"github.com/kokardy/listing"
)

var wg sync.WaitGroup

func convs(s string) []string {
	tmp := make([]string, len(s))
	for i, ix := range s {
		tmp[i] = string(ix)
	}
	return tmp
}

func producer(ctx context.Context, guess chan<- []byte) {
	defer close(guess)

	char := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123789/*-+$}{,=./"
	charset := listing.StringReplacer(convs(char))
	selectnum := 11
	repeatable := true
	buf := 100

	for perm := range listing.Permutations(charset, selectnum, repeatable, buf) {
		select {
		case <-ctx.Done():
			return
		default:
			var guessstr []string = perm.(listing.StringReplacer)
			var guessbyte []byte = []byte(strings.Join(guessstr, ""))
			guess <- guessbyte
		}
	}
}

func consumer(ctx context.Context, cancel func(), guess <-chan []byte, job int) {
	//can recreate another context.WithCancel but lazy
	defer wg.Done()
	defer cancel()
	for {
		select {
		case try := <-guess:
			sum := 1
			solution := ""
			for _, ix := range try {
				sum += int(ix)
				solution += string(ix)
			}
			//time.Sleep(5 * time.Millisecond)
			if sum%421 == 0 {
				fmt.Printf("[job %v] Solution %v\n", job, solution)
				cancel()
				return
			}
		case <-ctx.Done():
			fmt.Printf("Solution already found job %v return\n", job)
			return
		}
	}

}

func main() {
	guess := make(chan []byte)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go producer(ctx, guess)

	for i := 0; i < 50; i++ {
		wg.Add(1)
		go consumer(ctx, cancel, guess, i)
	}
	wg.Wait()
	fmt.Println("exit main")
}
