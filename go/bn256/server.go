package main

import (
	"crypto/rand"
	"fmt"
	"log"
	"math/big"

	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/bn256"
)

//Server : Using G1 for server
type Server struct {
	G1pubsrv *bn256.G1 //g1^s
	secret   *big.Int
}

//Identityclient : using G2 for client
type Identityclient struct {
	g2pubcli  *bn256.G2 //g2^q
	g2privcli *bn256.G2 //g2^qs
}

//Initsrv : Init secret and point for server
func Initsrv() *Server {
	fmt.Printf("Order of bn256 %v\n", bn256.Order)
	secret, err := rand.Int(rand.Reader, bn256.Order)
	if err != nil {
		log.Fatalf("fail to generate random number for server init %v", err)
	}
	return &Server{secret: secret, G1pubsrv: new(bn256.G1).ScalarBaseMult(secret)}
}

//ComputePrivateKey : Compute privatekey based on identity mail adress for example
func (srv *Server) ComputePrivateKey(identity []byte) *Identityclient {
	pubkey := hash2point(identity)
	privkey := new(bn256.G2).ScalarMult(pubkey, srv.secret)
	return &Identityclient{g2pubcli: pubkey, g2privcli: privkey}
}

//Getkeyforencrypt : generate symetric key to encrypt
func (srv *Server) Getkeyforencrypt(identity []byte) ([]byte, *bn256.G1) {
	pubkeyclient := hash2point(identity)              //g2^q
	pairing := bn256.Pair(srv.G1pubsrv, pubkeyclient) // e(g1^s,g2^q) = gt^sq

	randomness, err := rand.Int(rand.Reader, bn256.Order)
	if err != nil {
		log.Fatalf("failed to generate random in getkeyforencrypt %v", err)
	}
	randomPoint := new(bn256.G1).ScalarBaseMult(randomness)        //g1^r
	pairingRandom := new(bn256.GT).ScalarMult(pairing, randomness) //gt^sqr

	var keycomponent []byte
	keycomponent = append(keycomponent, pairingRandom.Marshal()...)
	keycomponent = append(keycomponent, randomPoint.Marshal()...)
	key := blake2b.Sum256(keycomponent)

	return key[:], randomPoint
}

//Getkeyfordecrypt : generate symetric key to decrypt
func (srv *Server) Getkeyfordecrypt(privatekeyuser *bn256.G2, randompoint *bn256.G1) []byte {
	pairingpoint := bn256.Pair(randompoint, privatekeyuser) //e(g1^r,g^qs) = gt^qsr
	var keycomponent []byte
	keycomponent = append(keycomponent, pairingpoint.Marshal()...)
	keycomponent = append(keycomponent, randompoint.Marshal()...)
	key := blake2b.Sum256(keycomponent)
	return key[:]
}
