package main

import (
	"encoding/base64"
	"fmt"
)

func main() {
	fmt.Printf("######## Playing pairing\n\n")
	srv := Initsrv()
	fmt.Printf("publicKey of srv %v\n\n", srv.G1pubsrv)

	fullidentityclient := srv.ComputePrivateKey([]byte("destinaire@dummy.example"))
	fmt.Printf("pubkey client : %v\n\nprivkey client : %v\n\n", fullidentityclient.g2pubcli, fullidentityclient.g2privcli)

	//Get the symetric key to use for perform encryption by using identity of the user
	key, randomPoint := srv.Getkeyforencrypt([]byte("destinaire@dummy.example"))
	fmt.Printf("Using key to encrypt destinaire@dummy.example\n%v\n\n", base64.StdEncoding.EncodeToString(key))

	//Retreive the symetric key to perform decryption by using privatekey and RandomPoint
	keyretreive := srv.Getkeyfordecrypt(fullidentityclient.g2privcli, randomPoint)
	fmt.Printf("Using key to Decrypt destinaire@dummy.example\n%v\n\n", base64.StdEncoding.EncodeToString(keyretreive))
}
