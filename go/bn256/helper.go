package main

import (
	"fmt"
	"math/big"

	"golang.org/x/crypto/blake2b"

	"golang.org/x/crypto/bn256"
)

// return hash(identity)*G2
func hash2point(identity []byte) *bn256.G2 {
	hash := blake2b.Sum256(identity)
	nhash := new(big.Int)
	nhash.SetBytes(hash[:])
	fmt.Printf("bigint of the identity :%v\n\n", nhash)
	Qpubpoint := new(bn256.G2).ScalarBaseMult(nhash)
	return Qpubpoint
}
