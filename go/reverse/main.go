package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type ReverseReader struct {
	reader io.Reader
}

func (r ReverseReader) Read(b []byte) (n int, err error) {
	n, err = r.reader.Read(b) //b contains the data read, we just have to reverse this slice
	lenb := n
	res := make([]byte, n)
	//function to reverse the slice
	for ; lenb > 0; lenb-- {
		res[n-lenb] = b[lenb-1]
	}
	copy(b, res)
	return n, err
}

func reverse(r io.Reader) io.Reader {
	return ReverseReader{r}
}

func main() {
	//test with String
	r := strings.NewReader("Hello, Reader!")
	reverseReaderString := reverse(r)

	b := make([]byte, 40)
	for {
		n, err := reverseReaderString.Read(b)
		if err == io.EOF {
			break
		}
		fmt.Printf("%q\n", b[:n])
	}

	//Test with file
	f, err := os.Open("test.junk")
	if err != nil {
		log.Fatalln(err)
	}
	reverseReaderFile := reverse(f)
	b = make([]byte, 1024)
	reverseReaderFile.Read(b)
	fmt.Printf("%q\n", b)

}
