# Context
It is just the beginning of my go adventure , and for the moment I focused on the channel, concurrency to do things faster and really enjoy the simplicity.
However, I was curious about the interface and how to use it:

1. "Classical way" to manage polymorphism (like vehicle include moto and car) => not interesting in this part and already well documented https://golangbot.com/interfaces-part-1/ and quiet easy understandable
* I was not comfortable about using the built in interface like io.Reader and how is it possible to chain them

# Goal
The example focus on the second parts, after reading
 https://medium.com/@matryer/golang-advent-calendar-day-seventeen-io-reader-in-depth-6f744bb4320b let's implement the reverse function.
Initially, as mentioned in the article, I would just implement type
func reverse (string) string

the beauty of Golang interface is that it is not more complicated to code the reverse function for any kind of input, file, string, and so on.

**Reminder**

```
Reader is the interface that wraps the basic Read method.

Read reads up to len(p) bytes into p. It returns the number of bytes read (0 <= n <= len(p)) and any error encountered. Even if Read returns n < len(p), it may use all of p as scratch space during the call. If some data is available but not len(p) bytes, Read conventionally returns what is available instead of waiting for more.

When Read encounters an error or end-of-file condition after successfully reading n > 0 bytes, it returns the number of bytes read. It may return the (non-nil) error from the same call or return the error (and n == 0) from a subsequent call. An instance of this general case is that a Reader returning a non-zero number of bytes at the end of the input stream may return either err == EOF or err == nil. The next Read should return 0, EOF.

Callers should always process the n > 0 bytes returned before considering the error err. Doing so correctly handles I/O errors that happen after reading some bytes and also both of the allowed EOF behaviors.

Implementations of Read are discouraged from returning a zero byte count with a nil error, except when len(p) == 0. Callers should treat a return of 0 and nil as indicating that nothing happened; in particular it does not indicate EOF.

Implementations must not retain p.

type Reader interface {
        Read(p []byte) (n int, err error)
}
```

All these package have already implemented the io.Reader interface, so the job is already done and we just need to apply the reverse function on the `slice b`

In case of strings.Reader which is an io.Reader, it means that strings implement `Read(p []byte) (n int, err error)`, we just need to get an io.Reader object
`r := strings.NewReader("Hello, Reader!")`


in our case, we define
```
func reverse(r io.Reader) io.Reader {
	return ReverseReader{r}
}
```

That means that ReverseReader struct have to implement `Read(p []byte) (n int, err error)`

```
type ReverseReader struct {
	reader io.Reader
}
```

here we go
```
func (r ReverseReader) Read(b []byte) (n int, err error) {
	n, err = r.reader.Read(b) //b contains the data read, we just have to reverse this slice
	lenb := n

	res := make([]byte, n)
  //function to reverse the slice
	for ; lenb > 0; lenb-- {
		res[n-lenb] = b[lenb-1]
	}
	copy(b, res)
	return n, err
}
```

# Use it

## string case

lets get an io.Reader on strings
```
r := strings.NewReader("Hello, Reader!")
reverseReaderString := reverse(r)
```

lets call Reader
```
b := make([]byte, 40)
reverseReaderString.Read(b)
fmt.Printf("%q\n", b)
```

output

`"!redaeR ,olleH"`
